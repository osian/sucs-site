<?
function make_password($length=8)
{
        $vowels = "aeiouy";
        $consonants = "bcdfghjklmnprst";
        $password = "";

        $cn = strlen($consonants)-1;
        $vn = strlen($vowels)-1;

        // Start on cons or vowel
        $alt = mt_rand(0, 1);

        // How many numbers
        $len = mt_rand($length-3,$length);

        for ($i = 0; $i < $len; $i++)
        {
                if ($alt == 1)
                {
                        $password .= $consonants[ mt_rand(0,$cn) ];
                        $alt = 0;
                }
                else
                {
                        $password .= $vowels[ mt_rand(0,$vn) ];
                        $alt = 1;
                }
        }
        for ($i = 0; $i < $length-$len; $i++)
        {
                $password .= mt_rand(0,9);
        }
        return $password;
}
function findUid($start, $end) {
	$ds=ldap_connect("localhost");
	ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
        $r=ldap_bind($ds);
        $sr=ldap_search($ds, "dc=sucs,dc=org", "uid=*",array(uidNumber));
        $info = ldap_get_entries($ds, $sr);
        for ($i = 0; $i < $info[count]; $i++) {
                $uids[$info[$i][uidnumber][0]] = true;
        }
        for ($i = $start; $i < $end; $i++) {
                if(!isset($uids[$i])) {
                        $safeuid = $i;
                        break;
                }
        }

        return $safeuid;
}

function generateLdif($uid, $password, $type, $realname, $username){
	// explode the realname
	$nameexplode = explode(' ', trim($realname));
	// hash the password
	$ldappassword = "{SHA}" . base64_encode(pack("H*", sha1($password)));
	// compile ldif
	$ldif  = "dn: uid=".$username.",ou=People,dc=sucs,dc=org\n";
	$ldif .= "uid: ".$username."\n";
	$ldif .= "cn: ".$realname."\n";
	// if only has 1 part to real name (and therefore a soc) then set it as sn otherwise set first name to given name and last name to sn
	if(count($nameexplode)>1){
	    $ldif .= "givenName: ".$nameexplode[0]."\n";
	    $ldif .= "sn: ".$nameexplode[count($nameexplode)-1]."\n";
	}
	else{
	    $ldif .= "sn: ".$realname."\n";
	}
	$ldif .= "mail: ".$username."@sucs.org\n";
	$ldif .= "objectClass: person\n";
	$ldif .= "objectClass: organizationalPerson\n";
	$ldif .= "objectClass: inetOrgPerson\n";
	$ldif .= "objectClass: posixAccount\n";
	$ldif .= "objectClass: top\n";
	$ldif .= "userPassword: ".$ldappassword. "\n";
	$ldif .= "loginShell: /bin/bash\n";
	$ldif .= "uidNumber: ".$uid."\n";
	// make some society specific changes
	// More like make sure peoples home dirs get made in the right place
	if($type==2){
	    $gid=1130;
	    $homebase="society";
	}
	elseif($type==5){
	    $gid=100;
	    $homebase="alumni";
	}
	elseif($type==3){
		$gid=100;
		$homebase="honorary";
	}
        elseif($type==4){
		$gid=100;
		$homebase="life";
	}
	else {
	    $gid=100;
	    $homebase="member";
	}
	$ldif .= "gidNumber: ".$gid."\n";
	$ldif .= "homeDirectory: /home/".$homebase."/".$username."\n";
	$ldif .= "gecos: ".$realname."\n\n";

	return $ldif;
}

?>
