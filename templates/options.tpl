{if $session->loggedin }
<!-- style="width: 80%; margin: auto;" was in here originally, why? -->
<div class="box" >
<div class="boxhead"><h2>User Settings</h2></div>
<div class="boxcontent">
	<fieldset>
	<legend> System Details </legend>
	<div class="row">
		<label for="username">Username:</label>
		<span class="textinput" id="username">
			{$session->username}
			<div class="note">If you <em>need</em> to change your username, please contact a member of admin.</div>
		</span>
	</div>
	<div class="row">
		<label for="fullname">Real name:</label>
		<span class="textinput">
			{$session->fullname}
			<div class="note">If your name has changed (e.g. through marriage), please contact a member of admin.</div>
		</span>
	</div>
	<div class="row">
		<label for="status">Membership status:</label>
		<span class="textinput">
			{$member.typename}
			{if $member.id == 1 || $member.id == 2}(Paid Until: {$member.paid}){/if}
		</span>
	</div>
	<div class="row">
		<label for="groups">Groups: </label>
		<span class="textinput"><span class="note"><a href="{$baseurl}/Knowledge/Help/Admin%20Tools/Groups">What are &ldquo;Groups&rdquo;?</a></span></span>
	</div>
	<div class="row">
		<span class="textinput" id="groups">
			<ul style="margin-top: 0; margin-bottom: 0;">
{foreach name=groups from=$session->groups key=grpname item=grpid}
				<li>{$grpname}</li>
{/foreach}
			</ul>
			<div class="note">To be added to or removed from groups, please contact a member of admin.</div>
		</span>
	</div>
	<div class="row">
		<label for="hackergotchi">Hackergotchi (user avatar):</label>
		<span class="textinput" id="hackergotchi">
{if $hackergotchi}
			<img src="{$baseurl}/pictures/people/{$session->username}.png" />
	<form action="{$baseurl}{$path}" method="post">
		<input type="hidden" name="action" value="clearhackergotchi" />
		<input type="submit" value="Clear Hackergotchi" />
	</form>

{else}
			No hackergotchi
{/if}
		
		</span>
		
	</div>

	<form method="post" action="{$baseurl}{$path}" enctype="multipart/form-data">
	<div class="row">
		<label>Update Hackergotchi:</label>
		<span class="textinput">
			<input name="hackergotchi" type="file" />
		<div class="note">See <a href="http://en.wikipedia.org/wiki/Hackergotchi">Wikipedia's entry on Hackergotchis</a> if you've not got a clue what we're on about!</div>
			<div class="note">This must be an image in PNG format with dimensions no greater than 128x128 pixels.</div>
		</span>
	</div>

	<div class="row">
		<input type="hidden" name="action" value="updatehackergotchi" />
		<input type="submit" value="Change" />
	</div>
	</form> 
	</fieldset>
	
	<form method="post" action="{$baseurl}{$path}">
	<fieldset>
	<legend> Password </legend>
	<input type="hidden" name="action" value="changepass" />
	<div class="row">
		<span class="textinput" id="password">
			<div class="row">
				<label>Old Password</label>
				<span class="textinput"><input type="password" name="oldpass" /></span>
			</div>
			<div class="row">
				<label>New Password</label>
				<span class="textinput">
				<input type="password" name="newpass1" />
				<div class="note">
					Passwords should be <em>at least</em> six characters long, 
					with upper/lowercase, punctuation, digits and all that jazz.
				</div> 
				</span>
			</div>
			<div class="row">
				<label>New Password (again)</label>
				<span class="textinput"><input type="password" name="newpass2" /></span>
			</div>
		</span>
	</div>
	<div class="row">
		<input type="submit" value="Change" />
	</div>
	</fieldset>
	</form>
	
	<form method="post" action="{$baseurl}{$path}">
	<fieldset>
	<legend> Contact Details </legend>
	<input type="hidden" name="action" value="changecontact" />
	<div class="row">
		<label for="address">Address:</label>
		<span class="textinput">
			<textarea id="address" name="address" rows="4" cols="40">{$member.address}</textarea>
		</span>
	</div>
	<div class="row">
		<label for="phone">Phone:</label>
		<span class="textinput">
			<input type="text" name="phone" id="phone" value="{$member.phone}" style="width: 95%;" />
		</span>
	</div>
	<div class="row">
		<label for="email">External Email:</label>
		<span class="textinput">
			<input type="text" name="email" id="email" value="{$member.email}" style="width: 95%;" />
			<div class="note">We are required by the Students' Union to keep contact details for our members. Please note that these details will be held in accordance with the Data Protection Act.</div>
		</span>
	</div>
	<div class="row">
		<input type="submit" value="Change" />
	</div>
	</fieldset>
	</form>

	<form method="post" action="{$baseurl}{$path}">
	<fieldset>
	<legend> Room Network Configuration </legend>
	<input type="hidden" name="action" value="changeguestnet" />
	<div class="row">
		<label for="guest_mac">Current GuestNet MAC:</label>
		<span class="textinput">
			<input type="text" name="currentGuestNetMAC" id="currentGuestNetMAC" value="{$currentGuestNetMAC}" style="width: 95%;"  disabled="disabled"/>
		</span>
	</div>
{if isset($newGuestNetMAC) }
	<div class="row">
		<label for="wireless_mac">New GuestNet MAC:</label>
		<span class="textinput">
			<input type="text" name="newGuestNetMAC" id="newGuestNetMAC" value="{$newGuestNetMAC}" style="width: 95%;" disabled="disabled" />
		</span>
		<span>
		You are currently connected from a different MAC address via GuestNet. If you would like you use this
		new MAC to access GuestNet, use the change button, but be aware you can only have 1 registered MAC
		address at a time.
		</span>
	</div>
	<div class="row">
		<input type="submit" value="Change" />
	</div>
{/if}
	</fieldset>
	</form>

{if ($member.type == 1 || $member.type == 2) && $member.paid != $paydate}
	<form method="post" action="{$baseurl}{$path}">
	<fieldset>
	<legend> Membership Renewal </legend>
	<input type="hidden" name="action" value="renew" />
	<div class="row">
		<label for="userid">ID:</label>
		<span class="textinput">
			<input type="text" name="userid" id="userid" value="" />
		</span>
		<label for="supass">Password:</label>
		<span class="textinput">
			<input type="text" name="supass" id="supass" value="" />
			<div class="note">You may use a purchased signup slip to renew your account by entering its details here.</div>
		</span>
	</div>
	<div class="row">
		<input type="submit" value="Renew" />
	</div>
	</fieldset>
	</form>
{/if}

	<div class="clear"></div>
</div>
<div class="hollowfoot"><div><div></div></div></div>
</div>
{else}
	<div class="errorbar">
		<div><div><div>
			You must be logged in to view or change your user settings.
		</div></div></div>
	</div>
{/if}
