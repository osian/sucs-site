{if $staff == TRUE}
<h2>DO NOT RENEW MEMBERSHIP UNLESS THE MONEY IS ACTUALLY IN THE POT!</h2>
<p>Pay until: {$paydate}</p>
<table border='1'>
<tr><th>Type</th><th>Username</th><th>Student Number</th><th>Real Name</th><th>Paid</th><th>Renew</th></tr>
{foreach name=members from=$members item=member}
<tr>
    <td>{$member.typename|escape}</td>
    <td>{$member.username|escape}</td>
    <td>{$member.sid|escape}</td>
    <td>{$member.realname|escape}</td>
    <td>{$member.paid|escape}</td>
    <td>
        <form action='{$self}' method='POST' style="margin-bottom: 0px;">
        <input type='hidden' name='uid' value='{$member.uid|escape}' />
        <input type='hidden' name='lastupdate' value='{$member.lastupdate|escape}' />
        <input type='submit' value='Renew'/>
        </form>
    </td>
    </tr>
{/foreach}
</table>
{else}
	<div class="errorbar">
		<div><div><div>
			You must be logged in and be a staff member to renew memberships.
		</div></div></div>
	</div>
{/if}
