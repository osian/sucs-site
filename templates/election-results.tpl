<h2>Election Results</h2>

{foreach name=outer key=position item=cand from=$candidate}
<h2>{$position|capitalize}</h2>
{if count($cand) > 1}
<table>
<tr>
	<th>Candidate</th>
	<th>Votes</th>
</tr>

{foreach item=person from=$cand}
<tr>
	<td><a href="{$person.manifesto_link}">{$person.realname}</a> ({$person.username})</td>
	<td style="text-align: right;">{$person.votes}</td>
</tr>
{/foreach}

</table>

{else}
There is no vote for {$position|capitalize} as only one candidate was nominated:<br />
<a href="{$cand.0.manifesto_link}">{$cand.0.realname}</a> ({$cand.0.username})<br />
{/if}

{/foreach}
