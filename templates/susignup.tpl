{if $mode=='login'}
<form action="{$componentpath}" method="post">
    <div class="box" style="width: 70%; margin: auto;">
        <div class="boxhead"><h2>Membership Signup</h2></div>
        <div class="boxcontent">

           <p>Please enter your Transation ID. It can be found by logging into <a href='http://www.swansea-union.co.uk/shop/purchasehistory/'>swansea-union.com</a> and selecting purchase history.</p>

            <div class="row">
                <label for="sid">Student Number:</label>
                <span class="textinput"><input type="text" size="20" name="sid" id="sid" /></span>
            </div>
            <div class="row">
                <label for="transactionID">Transaction ID:</label>
                <span class="textinput"><input type="text" size="20" name="transactionID" id="transactionID" /></span>
            </div>
            <div class="row"><span class="textinput">
                <input type="submit" name="submit" value="Join" /></span>
            </div>
            <div class="clear"></div>
            <div class="note">If you already have an account and wish to renew, simply buy an additional years membership from the <a href='http://www.swansea-union.co.uk/mysociety/sucs/'>SUSU website</a>. Enter the details above and click "Join"</div>
        </div>
        <div class="hollowfoot"><div><div></div></div></div>
    </div>
</form>
{elseif $mode=='form' || $mode=="re-form"}
	<h1>Signup</h1>
		<p>To continue signup later please use the link below to dispay and print off your signup slip</p>
		<form action="https://sucs.org/~kais58/sucstest/sucs/generate.php" method="post" target="_blank">
			<input type=hidden name="id" id="id" value="{$id}" />
			<input type=hidden name="pass" id="pass" value="{$pass}" />
			<input type=submit name="submit" value="Proceed" />
		</form>
		<p>To finish signup now click below to continue</p>
		<form action="https://sucs.org/signup/" method="post">
			<input type=hidden name="signupid" id="id" value="{$id}" />
			<input type=hidden name="signuppw" id="pass" value="{$pass}" />
			<input type=submit name="submit" value="Proceed" />
		</form>
{elseif $mode=='numpty'}
		<p>You appear to have completed this part of signup before, please check your emails for your signup ID and password. Alternatively click below to continue.</p>
		<form action="https://sucs.org/signup/" method="post">
			<input type=hidden name="signupid" id="id" value="{$id}" />
			<input type=hidden name="signuppw" id="pass" value="{$pass}" />
			<input type=submit name="submit" value="Proceed" />
		</form>
{elseif $mode=='numpty2'}
		<p>You appear to have already completed this step and registered with SUCS with the username <strong>{$username}</strong>.<br/>If you have forgotten your login password please email <a href='mailto:admin@sucs.org'>admin@sucs.org</a> or drop by the SUCS room and find an admin.</p>
		
{elseif $mode=='renew'}
	<h1>Renewed</h1>
	<p>Thankyou for renewing your membership, it has completed succesfully</p>
{else}
<h1>Error</h1>
        <div class='errorbar'>
                <strong>Error: </strong> {$error_text}
        </div>
An error occured during signup, please email, with as much information as you can provide, <a href='mailto:admin@sucs.org'>admin@sucs.org</a> for assistance.
{/if}

