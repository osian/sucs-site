<div class="cbb">
<h3>Current Milliways Users</h3>
{if $people|@count < 1}
<p>No-one active on Milliways</p>
{else}
<ul>
    {foreach name=people from=$people key=personnum item=person}
    <li>{$person.username} <small>({$person.idle} idle)</small></li>
    {/foreach}
</ul>
{/if}
</div>

<div class="cbb">
<h3><a href="{$mwpath}/URI">Recent URIs</a></h3>
<ul>
    {foreach name=list from=$urilist item=uri}
    <li style="white-space: nowrap">{$uri.user} <small><a href="{$uri.url|escape:'html'}" title="{$uri.title|escape:'htmlall'}">{$uri.title|truncate:18:"..":true:false|escape:'htmlall'}</a></small></li>
    {/foreach} 
</ul>
</div>

<div class="cbb">
<h3><a href="{$mwpath}/tag">Recent TAGs</a></h3>
<ul>
    {foreach name=taglist from=$taglist item=tag}
    <li>{$tag.user} <a href="{$mwpath}/tag/{$tag.name|escape:'url'}">{$tag.tag}</a></li>
    {/foreach} 
</ul>
</div>
