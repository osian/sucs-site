<h2>Getting Started</h2>
<p>Congratulations on becoming a SUCS member! But what can you do with your new-found membership? Why not try one of the following?</p>
<ul>
<li>Chat with us on our <a href="Community/Milliways" title="Milliways, the SUCS talker">chat room</a></li>
<li>Come to a social [JCs, Wednesdays, 1pm]</li>
<li>Visit <a href="About/Room" title="The SUCS room">the room</a></li>
<li>Find out <a href="News/" title="SUCS news">what's happening</a></li>
<li>Log in <a href="Knowledge/Help/SUCS%20Services/Logging%20in%20remotely" title="Logging in remotely">remotely</a></li>
<li>Access your <a href="Knowledge/Help/SUCS%20Services/Using%20WebDAV" title="Accessing your files remotely with WebDAV">disk space</a></li>
<li>Host your <a href="Knowledge/FAQ#s_s4" title="SUCS FAQ: Where's my website?">website</a> with us</li>
<li>Play some <a href="Games" title="The SUCS game server">games</a></li>
<li>Browse our <a href="Knowledge/Library" title="The SUCS library">library</a></li>
<li>Check out a <a href="Community/Projects" title="SUCS Projects">project</a> (or start your own)</li>
<li>Join our development <a href="http://lists.sucs.org/mailman/listinfo/devel" title="SUCS Devel mailing list">mailing list</a> (for our website, computer system, etc)</li>
</ul>