<div class="box">
<div class="boxhead">
<h2>Rhodri Curnow (mistake) - "Bertrand Russell"</h2>
</div>
<div class="boxcontent">
<p>"A stupid man's report of what a clever man says can never be accurate, because he unconsciously translates what he hears into something he can understand."</p>
<ul>
<li><a href="/videos/talks/2011-11-01/mistake.pdf">Download the slides</a></li>
</ul>
<div id="player">
<object data="/videos/talks/mediaplayer.swf?file=2011-11-01/mistake.flv" height="275" id="player" type="application/x-shockwave-flash" width="320">
<param name="height" value="256" />
<param name="width" value="320" />
<param name="file" value="/videos/talks/2011-11-01/mistake.flv" />
<param name="image" value="/videos/talks/2011-11-01/mistake.png" />
<param name="id" value="player" />
<param name="displayheight" value="256" />
<param name="FlashVars" value="image=/videos/talks/2011-11-01/mistake.png" />
</object>
</div>
<p><strong>Length: </strong>10m 10s</p>
<p><strong>Video: </strong><a href="/videos/talks/2011-11-01/mistake.ogv" title="720x576 Ogg
Theora - 17MB">720x576</a> (Ogg Theora, 17MB)</p>
</div>
<div class="boxfoot">
<p>&nbsp;</p>
</div>
</div>
