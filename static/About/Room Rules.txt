<h2>Rules of the Room</h2>
<p>Version: 1.0<br />
Date: Michaelmas term 2000</p>
<p><strong>NOTE:</strong> The following rules are relevant to a members use of the SUCS Computer Room, herein known as 'the room'. All rules are in addition to the societies general terms and conditions, breech of these rules will result in a members rights to use the room being withdrawn, and potentially their society membership status being reviewed, or other appropriate actions.</p>
<h3>General Rules</h3>
<ol>
<li>Membership of the Society does not imply the right to use the room, all members are initially permitted to use the room, but that right can be withdrawn by a member of admin if deemed necessary.</li>
<li>All users of the room must respect others use of the facilities. You should not disturb other people unduly, and honour all reasonable requests to abate any disruptive activities.</li>
<li>Respect other users rights to privacy, due to the cramped conditions it is not always possible for a user to totally prevent another from viewing their screen, therefore you must accept that objection may be taken at anyone doing so.</li>
<li>No Sleeping in the room.</li>
<li>Do not harass other users, or the general public. The laws of the land still apply inside the room, this includes Sexual Harassment, Indecent Exposure, and all other socially obnoxious behaviour.</li>
<li>ALL items left in the room are done so entirely at the owners own risk, the society will accept absolutely no responsibility for loss, theft, damage or otherwise to anything left in the room, regardless if you have asked permission or not.</li>
<li>No Littering. Anyone found to be leaving litter in the room, or in any way making a mess will be dealt with by a member of admin in a manner deemed appropriate, anything from being forced to clean the room, to being barred.</li>
<li>The society admin reserve the right to refuse admittance to the room at any person they suspect of potential causing disruption, offence, or is suspected of breaking any of the other rules.</li>
<li>Users are not to interfere with, or damage any equipment in the room. This includes resetting, turning off, or unplugging  any computers that you were not authorised to do so. Hitting, bashing, or moving any equipment, etc.</li>
</ol>
<h3>Use of the 'Guest' Network</h3>
<ol>
<li>Only devices which have been approved by, and registered with a member of admin may be attached to the guest network.</li>
<li>Any equipment that is deemed to be unsafe, or unsuitable for use on the network may be removed without notice.</li>
<li>All Equipment left in the room is done so entirely at the owners risk, I know we said this already, but we <strong>really</strong> mean it.</li>
<li>The Society Rules of Inappropriate file content still apply to your own machines, ie no illegal or dubious software, music, movies, etc. No pornographic or 'adult' material either.</li>
<li>No Servers. You are not to run any kind of file or other server type service that offers access to the general public without explicit consent from a member of admin.</li>
<li>Any computer left in the room for a significant length of time must be available for use as a general SUCS Terminal if requested. A member of admin can tell you how to set this up securely.</li>
<li>Any equipment found being used for 'inappropriate purposes' will be disconnected, or blocked from the network, and its owner or operator will be appropriately disciplined.</li>
</ol>