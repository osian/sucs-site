<?php 
$uritable="shorturi";

$chrs = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S' ,'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
$output = "";

function int_to_alph($int, $chrs) {
	$int = (int) $int;
	$base = (int) sizeof($chrs);
	$alph = "";
	do {
		$alph = $chrs[($int % $base)] . $alph;
	} while($int = intval($int / $base));
	return $alph;
}

function alph_to_int($alph, $chrs) {
	$base = sizeof($chrs);
	for($i = 0, $int = 0; $i < strlen($alph); $i++) {
		$int += intval(array_search(substr($alph, strlen($alph) - $i - 1, 1), $chrs)) * pow($base, $i);
	}
	return (int) $int;
}

if (isset($pathlist[3])) {
  $url=$DB->GetOne("select url from $uritable where id='".alph_to_int($pathlist[3], $chrs)."'");

	if ($url) {
		header('Location: '.$url);
	} else {
		echo "URL not found";
	}

} else {
	if ($session->loggedin) {
		if (@$_REQUEST['action']) {
			$shorturi=$DB->GetOne("select id from $uritable where url=?", array(@$_REQUEST['uri']));
			if (!$shorturi) {
				$headers=get_headers($_REQUEST['uri'], 1);
				if ($headers) {
					if (preg_match("/ 4/", $headers[0])) {
						trigger_error("HTTP 4xx error detected - not creating ShortURI", E_USER_WARNING);
					} else {				
						$record['url'] = @$_REQUEST['uri'];
						$record['creator'] = $session->username;
						$record['created'] = "now";
						$DB->AutoExecute($uritable, $record, 'INSERT');
						$shorturi=$DB->GetOne("select id from $uritable where url=?", array(@$_REQUEST['uri']));
					}
				} else {
					trigger_error("URI supplied is not valid", E_USER_WARNING);
				}
			}

			if ($shorturi>0) $smarty->assign("uri", "http://".$_SERVER['SERVER_NAME']."/uri/".int_to_alph($shorturi, $chrs));
		}

	} else {
		trigger_error("You are not logged in", E_USER_WARNING);
	}
}

$output = $smarty->fetch('uri.tpl');

$smarty->assign("title", "ShortURI");
$smarty->assign("body", $output);

?>
